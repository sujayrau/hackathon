from flask import Flask, render_template, request, url_for, redirect, send_from_directory
from werkzeug import secure_filename
from flask.ext.sqlalchemy import SQLAlchemy
import os
import datetime,random,urllib2,re
app = Flask(__name__)
from models import *
app.config['UPLOAD_FOLDER'] = 'static/files'


@app.route('/', methods=['GET', 'POST'])
def index():
    random_imgs = {}
    categories = {}
    for department in db.session.query(Courses.dept).distinct():
        random_imgs[department[0]] = getInterestingFlickrImage(department[0]);
        courses_array = []
        for id_name in db.session.query(Courses.num, Courses.name, Courses.id).filter(Courses.dept == department[0]):
            course_name = str(id_name[0]) + ' - ' + id_name[1]
            courses_array += [(course_name, 'http://localhost:5000/' + str(id_name[2]))]
        categories[department[0]] = courses_array

#    categories = {'Computer Science':['CS262','CS261','CS383'], 'Art':['Art 1','Art 2','Art 3']}

    return render_template('index.html', random_pics=random_imgs, categories=categories, random=jinja_rand)


@app.route('/add_course', methods=['GET', 'POST'])
def add_course():
    department = request.form['department']
    number = request.form['number']
    name = request.form['name']
    add_row(Courses, department, number, name)
    return redirect(url_for('index'))

@app.route('/upvote/<item_id>/<course_id>')
def upvote(item_id, course_id):
    item = Items.query.filter(Items.id == item_id).first()
    item.rating += 1
    db.session.commit()
    return redirect('http://localhost:5000/' + course_id)

@app.route('/downvote/<item_id>/<course_id>')
def downvote(item_id, course_id):
    item = Items.query.filter(Items.id == item_id).first()
    item.rating -= 1
    db.session.commit()
    return redirect('http://localhost:5000/' + course_id)


@app.route('/<course_id>')
def course(course_id):
    lectures = []
    i = 1
    for Lecture_data in db.session.query(Lectures.name, Lectures.id).filter(Lectures.course_id == course_id):
        items = []
        for Item in db.session.query(Items.item_loc, Items.rating, Items.id, Items.title).filter(Items.lecture_id == Lecture_data[1]).order_by(Items.rating * -1):
            item =[Item[0], Item[1], Item[2], Item[3]]
            items += [item]
        lectures += [('Lecture ' + str(i), Lecture_data[0], items, Lecture_data[1])]
        i+=1
        start_val = 0
        try:
            start_val = lectures[0][3]
        except:
            start_val = 0
    return render_template('course.html', lectures=lectures, course_id=course_id, start_val=start_val)

@app.route('/upload', methods=['POST'])
def upload_file():
    lecture_id = int(request.form['dog'])
    title = request.form['title']
    course_id = request.form['cat']
    if request.method == 'POST':
        file = request.files['file']
        if file:
            filename = secure_filename(file.filename)
            if not os.path.exists(app.config['UPLOAD_FOLDER']):
            	os.makedirs(app.config['UPLOAD_FOLDER'])
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
    add_row(Items,
            lecture_id,os.path.join(app.config['UPLOAD_FOLDER'], filename),
            2,
            title)
    return redirect('http://localhost:5000/' + course_id)

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['UPLOAD_FOLDER'],
                               filename)

#inserting rows into the database
def add_row(table,*args):
    if len(args) == 1:
        x = table(args[0])
        db.session.add(x)
    if len(args) == 2:
        x = table(args[0],args[1])
        db.session.add(x)
    if len(args) == 3:
        x = table(args[0],args[1],args[2])
        db.session.add(x)
    if len(args) == 4:
        x = table(args[0],args[1],args[2],args[3])
        db.session.add(x)
    db.session.commit()

def getInterestingFlickrImage(dept):
    ''' Returns a random "interesting" image from Flickr.com.
        The image is saved in current directory.
        
        In case the image is not valid (eg.photo not available, etc.)
        the image is not saved and None is returned.
    
        Input:
            filename (string): An optional filename.
                 If filename is not provided, a name will be automatically provided.
            None
        
        Output:
            (string) Name of the file.
                     None if the image is not available.
    '''
    # Choose a random date between the beginning of flickr and yesterday.
   
    # Get a random page for this date.
    url = 'http://flickr.com/search/?q=' + dept
    urlfile = urllib2.urlopen(url)
    html = urlfile.read(500000)
    urlfile.close()
    # Extract images URLs from this page
    re_imageurl = re.compile('src="http.*\.jpg',re.IGNORECASE)
    urls = re_imageurl.findall(html)
    if len(urls)==0:
        raise ValueError,"Oops... could not find images URL in this page. Either Flickr has problem, or the website has changed."
    # Choose a random image
    return urls
    
def jinja_rand(array):
    tmp = random.choice(array)
    array.remove(tmp)
    return tmp

@app.route('/kanye')
def kanye():
    return render_template('kanye.html')

if __name__ == '__main__':
    app.run(debug=True)
