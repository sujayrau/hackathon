from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

from index import app
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)

class Courses(db.Model):
    #__tablename__ = 'course'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    dept = db.Column(db.String(120))
    num = db.Column(db.String(120))

    def __init__(self, dept, num, name):
        self.name = name
        self.dept = dept
        self.num = num

class Lectures(db.Model):
    #__tablename__ = 'lecture'
    id = db.Column(db.Integer, primary_key=True)
    course_id = db.Column(db.Integer, db.ForeignKey(Courses.id))
    name = db.Column(db.String(120))

    def __init__(self, c_id, name):
        self.course_id = c_id
        self.name = name

        
class Users(db.Model):
    #__tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(240))

    def __init__(self, name):
        self.name = name


class Items(db.Model):
    #__tablename__ = 'item'
    id = db.Column(db.Integer, primary_key=True)
    lecture_id = db.Column(db.Integer, db.ForeignKey(Lectures.id))
    rating = db.Column(db.Integer)
    item_loc = db.Column(db.String(240))
    user_id = db.Column(db.Integer, db.ForeignKey(Users.id))
    title = db.Column(db.String(240))

    def __init__(self, l_id, item_loc, u_id, title):
        self.lecture_id = l_id
        self.item_loc = item_loc
        self.user_id = u_id
        self.rating = 0
        self.title = title

